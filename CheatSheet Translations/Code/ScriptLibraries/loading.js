dojo.require('dijit.Dialog');
function loading() {
   underlay = new dijit.DialogUnderlay({'class': 'loading'});
   underlay.show();
}

function mask() {
	underlay = new dijit.DialogUnderlay();
	underlay.show();
}

function stoploading(){
   underlay.hide();
}

function checkLogin() {
   dojo.xhrGet({
     url : window.location.href.split('.nsf')[0] + ".nsf?opendatabase",
     handleAs : 'text',
     preventCache : true, 
     load : function(response, ioArgs) {
       if (response.indexOf('action="/names.nsf?Login"')!=-1){
         clearInterval();
         window.location.href = window.location.href.split('.nsf')[0] + ".nsf?Login&RedirectTo=" + window.location.href
       } 
     },
     error : function(response, ioArgs) {
        // Do any custom error handling here that you want to.
     }
  })
};

// Adding code to check timeout every 16 minutes and prompt login if nothing is done
dojo.addOnLoad(function(){
    setInterval("checkLogin()", 960000);
});