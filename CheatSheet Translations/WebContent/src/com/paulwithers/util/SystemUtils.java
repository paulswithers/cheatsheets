package com.paulwithers.util;

import java.util.Vector;

import com.ibm.xsp.extlib.beans.UserBean;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class SystemUtils {

	public static int getAccess() {
		int retVal_ = 0;
		try {
			UserBean currUserBean = UserBean.get();
			if (!currUserBean.isAuthenticatedUser()) {
				retVal_ = 0;
			} else {
				String userName = ExtLibUtil.getCurrentSession().getEffectiveUserName();
				Vector roles = ExtLibUtil.getCurrentDatabase().queryAccessRoles(userName);
				if (roles.contains("[Admin]")) {
					retVal_ = 2;
				} else {
					retVal_ = 1;
				}
			}
		} catch (Exception e) {
			CSV_Util.getOpenLogItem().logError(e);
		}
		return retVal_;
	}

}
