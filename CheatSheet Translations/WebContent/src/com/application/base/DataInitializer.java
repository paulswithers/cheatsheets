package com.application.base;

import java.io.InputStream;

import com.paulwithers.util.CSV_Util;

public class DataInitializer {

	boolean deleteAllDocs;

	public DataInitializer() {

	}

	public boolean loadAllEntries() {

		boolean retVal = false;
		InputStream cs = DataInitializer.class.getResourceAsStream("CheatSheets");
		InputStream csMapper = DataInitializer.class.getResourceAsStream("CheatSheetsMapper");
		retVal = (CSV_Util.loadDocsFromFile("CheatSheets", cs, csMapper));
		return retVal;
	}

}