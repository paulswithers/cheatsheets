package com.application.base;

import java.io.Serializable;
import java.util.ArrayList;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewNavigator;

import org.apache.commons.lang.StringUtils;

import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.paulwithers.util.CSV_Util;

public class CheatSheetManager implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String cheatsheet;
	private String lang;
	private String page;
	private ArrayList<String> cheatsheetOptions;
	private ArrayList<CheatSheet> entries = null;

	public String getCheatsheet() {
		return cheatsheet;
	}

	public void setCheatsheet(String cheatsheet) {
		if (!StringUtils.equals(this.cheatsheet, cheatsheet)) {
			this.cheatsheet = cheatsheet;
			setEntries();
		} else {
			this.cheatsheet = cheatsheet;
		}
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		if (!StringUtils.equals(this.lang, lang)) {
			this.lang = lang;
			setEntries();
		} else {
			setEntries();
		}
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		if (!StringUtils.equals(this.page, page)) {
			this.page = page;
			setEntries();
		} else {
			this.page = page;
		}
	}

	public ArrayList<String> getCheatsheetOptions() {
		if (null == cheatsheetOptions) {
			setCheatsheetOptions();
		}
		return cheatsheetOptions;
	}

	public void setCheatsheetOptions() {
		try {
			Database currDb = ExtLibUtil.getCurrentDatabase();
			View vwTrans = currDb.getView("defaultView");
			ViewNavigator navTrans = vwTrans.createViewNav();
			if (null != cheatsheetOptions) {
				cheatsheetOptions.clear();
			} else {
				cheatsheetOptions = new java.util.ArrayList<String>();
			}
			ViewEntry entTrans = navTrans.getFirst();
			while (null != entTrans) {
				cheatsheetOptions.add((String) entTrans.getColumnValues().get(0));
				ViewEntry tmp = navTrans.getNextSibling();
				entTrans.recycle();
				entTrans = tmp;
			}
		} catch (Exception e) {
			CSV_Util.getOpenLogItem().logError(e);
		}
	}

	public ArrayList<CheatSheet> getEntries() {
		if (null == entries) {
			setEntries();
		}
		return entries;
	}

	public void setEntries() {
		try {
			if (StringUtils.isBlank(getLang()) || StringUtils.isBlank(getCheatsheet())
					|| StringUtils.isBlank(getPage())) {
				return;
			}
			Database currDb = ExtLibUtil.getCurrentDatabase();
			View vwTrans = currDb.getView("defaultView");
			String cat = getCheatsheet() + "\\" + getPage();
			ViewNavigator navTrans = vwTrans.createViewNavFromCategory(cat);
			if (null != entries) {
				entries.clear();
			} else {
				entries = new java.util.ArrayList<CheatSheet>();
			}
			ViewEntry entTrans = navTrans.getFirstDocument();
			while (null != entTrans) {
				CheatSheet cs = new CheatSheet();
				cs.loadDoc(entTrans, getLang());
				entries.add(cs);
				ViewEntry tmp = navTrans.getNext();
				entTrans.recycle();
				entTrans = tmp;
			}
		} catch (NotesException e) {
			CSV_Util.getOpenLogItem().logError(e);
		} catch (Exception e) {
			CSV_Util.getOpenLogItem().logError(e);
		}
	}

	public boolean saveEntries() {
		boolean retVal_ = false;
		try {
			boolean allSucc = true;
			for (CheatSheet c : entries) {
				if (!c.save(getLang())) {
					allSucc = false;
				}
			}
			if (allSucc)
				retVal_ = true;
		} catch (Exception e) {
			CSV_Util.getOpenLogItem().logError(e);
		}
		return retVal_;
	}
}