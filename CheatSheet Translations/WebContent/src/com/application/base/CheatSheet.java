package com.application.base;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.Name;
import lotus.domino.NotesException;
import lotus.domino.ViewEntry;

import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.paulwithers.util.CSV_Util;

public class CheatSheet implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String unid;
	private String page;
	private String english;
	private String altLang;
	private String details;
	private String translateDetails;
	private boolean saved;

	public String getUnid() {
		return unid;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getEnglish() {
		return english;
	}

	public void setEnglish(String english) {
		this.english = english;
	}

	public String getAltLang() {
		return altLang;
	}

	public void setAltLang(String altLang) {
		this.altLang = altLang;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getTranslateDetails() {
		return translateDetails;
	}

	public void setTranslateDetails(String translateDetails) {
		this.translateDetails = translateDetails;
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public void loadDoc(ViewEntry srcEnt, String lang) {
		try {
			// Set values from view entry
			setUnid(srcEnt.getUniversalID());
			setPage((String) srcEnt.getColumnValues().get(1));
			setEnglish((String) srcEnt.getColumnValues().get(2));
			setDetails((String) srcEnt.getColumnValues().get(3));

			// now get source doc and get alt lang value
			Database currDb = ExtLibUtil.getCurrentDatabase();
			Document srcDoc = currDb.getDocumentByUNID(getUnid());
			if (srcDoc.hasItem("Text" + lang)) {
				setAltLang(srcDoc.getFirstItem("Text" + lang).getText());
			} else {
				setAltLang("");
			}
			if (srcDoc.hasItem("LastTranslated" + lang) && srcDoc.hasItem("LastTranslator" + lang)) {
				setTranslateDetails(srcDoc.getFirstItem("LastTranslator" + lang).getText() + " - "
						+ srcDoc.getFirstItem("LastTranslated" + lang).getText());
			} else {
				setTranslateDetails("");
			}
			setSaved(false);
		} catch (NotesException e) {
			CSV_Util.getOpenLogItem().logError(e);
		} catch (Exception e) {
			CSV_Util.getOpenLogItem().logError(e);
		}
	}

	public boolean save(String lang) {
		setSaved(false);
		try {
			// now get source doc and get alt lang value
			Database currDb = ExtLibUtil.getCurrentDatabase();
			Document srcDoc = currDb.getDocumentByUNID(getUnid());
			String fieldName = "Text" + lang;
			if (srcDoc.hasItem(fieldName)) {
				if (!StringUtils.equals(srcDoc.getFirstItem(fieldName).getText(), getAltLang())) {
					srcDoc.replaceItemValue(fieldName, getAltLang());
				} else {
					return true;
				}
			} else {
				srcDoc.replaceItemValue(fieldName, getAltLang());
			}
			Name cname = ExtLibUtil.getCurrentSession().createName(
					ExtLibUtil.getCurrentSession().getEffectiveUserName());
			srcDoc.replaceItemValue("LastTranslator" + lang, cname.getCommon());
			srcDoc.replaceItemValue("LastTranslated" + lang, new Date().toString());
			setTranslateDetails(srcDoc.getFirstItem("LastTranslator" + lang).getText() + " - "
					+ srcDoc.getFirstItem("LastTranslated" + lang).getText());
			srcDoc.save(true, false);
			setSaved(true);
		} catch (Exception e) {
			CSV_Util.getOpenLogItem().logError(e);
		}
		return isSaved();
	}
}